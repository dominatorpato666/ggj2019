﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Room : MonoBehaviour {

    //references
    [SerializeField]
    private new Rigidbody2D rigidbody2D;

    //other references
    [SerializeField]
    private GameObject floor;
    [SerializeField]
    private GameObject wallL;
    [SerializeField]
    private GameObject wallR;
    [SerializeField]
    private GameObject Bg;
    [SerializeField]
    private SpriteRenderer bgSpriteRenderer;
    [SerializeField]
    private Sprite[] bgSpritesP1;
    [SerializeField]
    private Sprite[] bgSpritesP2;

    //tweakers
    [SerializeField]
    private int exitSpeed;

    private const float CHARGE_ANIM_DISTANCE = 4;

    private const float ITEMS_FALL_ANIM_OFFSET = 11;

    //private state
    private Vector2 exitDirection;
    private MpGameplayController.PlayerID ownerID;

    public enum ROOM_TYPE
    {
        KITCHEN,
        BATH,
        BED,
        LIVING
    }

    public ROOM_TYPE roomType;

    public void SetOwnerID(MpGameplayController.PlayerID ownerID)
    {
        this.ownerID = ownerID;
        if(ownerID == MpGameplayController.PlayerID.P1)
        {
            exitDirection = Vector2.left;
        }
        else
        {
            exitDirection = Vector2.right;
        }
    }

    public void StartSpawnAnimation()
    {
        if (ownerID == MpGameplayController.PlayerID.P1) {
            bgSpriteRenderer.sprite = bgSpritesP1[(int)roomType];
        }
        else
        {
            bgSpriteRenderer.sprite = bgSpritesP2[(int)roomType];
        }

        AnimateRoomAssemble();
    }

    private void AnimateRoomAssemble()
    {
        //Animate!
        //Target Positions
        Vector2 floor_Target = floor.transform.position;
        Vector2 wallL_Target = wallL.transform.position;
        Vector2 wallR_Target = wallR.transform.position;
        Vector2 Bg_Target = Bg.transform.position;
        //Now position above target and animate!
        floor.transform.position = floor_Target + Vector2.up * ITEMS_FALL_ANIM_OFFSET;
        wallL.transform.position = wallL_Target + Vector2.up * ITEMS_FALL_ANIM_OFFSET;
        wallR.transform.position = wallR_Target + Vector2.up * ITEMS_FALL_ANIM_OFFSET;
        Bg.transform.position = Bg_Target + Vector2.up * ITEMS_FALL_ANIM_OFFSET;
        //durations
        float time0 = Random.Range(0.3f, 0.7f);
        float time1 = Random.Range(0.3f, 0.7f);
        float time2 = Random.Range(0.3f, 0.7f);
        float time3 = Random.Range(0.5f, 0.8f);
        //aNIMATE
        floor.transform.DOMove(floor_Target, time0).easePeriod = 0;
        wallL.transform.DOMove(wallL_Target, time1).easePeriod = 0;
        wallR.transform.DOMove(wallR_Target, time2).easePeriod = 0;
        Bg.transform.DOMove(Bg_Target, time3).easePeriod = 0;
        //fades
        floor.GetComponent<SpriteRenderer>().DOFade(1, time0);
        wallL.GetComponent<SpriteRenderer>().DOFade(1, time1);
        wallR.GetComponent<SpriteRenderer>().DOFade(1, time2);
        Bg.GetComponent<SpriteRenderer>().DOFade(1, time3);
    }

    public void StartExitAnimation()
    {
        //rigidbody2D.velocity = exitDirection * exitSpeed;
        rigidbody2D.DOMove((Vector2)transform.position -exitDirection * CHARGE_ANIM_DISTANCE, 0.3f);
        if(exitDirection == Vector2.right)
        {
            transform.DORotate(new Vector3(0, 0, Random.Range(5, 10)), 0.3f);
        }
        else
        {
            transform.DORotate(new Vector3(0, 0, Random.Range(-5, -10)), 0.3f);
        }
        StartCoroutine(ContinueExitAnimation());
    }

    private IEnumerator ContinueExitAnimation()
    {
        yield return new WaitForSeconds(0.3f);
        transform.DORotate(Vector3.zero, 0.3f);
        rigidbody2D.velocity = exitDirection * exitSpeed;
        Destroy(gameObject, 4);
    }

    public ROOM_TYPE GetRoomType()
    {
        return roomType;
    }

    public void SetRoomType(ROOM_TYPE type)
    {
        roomType = type;
    }

}

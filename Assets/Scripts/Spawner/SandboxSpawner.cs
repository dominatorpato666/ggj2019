﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public sealed class SandboxSpawner : MonoBehaviour {

    [SerializeField]
    private HouseItemsCatalog m_houseItemsCatalog;

    //to spawn a given item
    [SerializeField]
    private bool m_forceSpawnItem;
    [SerializeField]
    private HouseItem m_itemToForceSpawn;

    [SerializeField] List<HouseItem> m_houseItemInstancesHistory;
    [SerializeField] List<GameObject> m_gameObjectInstancesHistory;

    void Start()
    {
        m_houseItemInstancesHistory = new List<HouseItem>();

        //TODO: Subscribe to onLevelComplete event
        //<script>.onLevelCompleted += OnLevelCompleted;
    }
    
    public void SpawnRandomItem()
    {
        if(m_forceSpawnItem)
        {
            Assert.IsNotNull(m_itemToForceSpawn);
            Instantiate(m_itemToForceSpawn.gameObject, transform);
        }
        else
        {
            m_houseItemInstancesHistory.Add(m_houseItemsCatalog.GetRandomHouseItem());
            int lastIndex = m_houseItemInstancesHistory.Count - 1;
            m_gameObjectInstancesHistory.Add(Instantiate(m_houseItemInstancesHistory[lastIndex].gameObject, transform));
        }
    }

    public List<HouseItem> GetHouseItemList()
    {
        return m_houseItemInstancesHistory;
    }

    public List<GameObject> GetInstancedObjectsList()
    {
        return m_gameObjectInstancesHistory;
    }

    public void OnLevelCompleted()
    {
        m_houseItemInstancesHistory.Clear();
        m_gameObjectInstancesHistory.Clear();
    }

#if DEBUG
    private void Update()
    {
        if(Input.GetKeyUp(KeyCode.Space))
        {
            SpawnRandomItem();
        }
    }
#endif
    
}

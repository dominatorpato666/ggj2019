﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class MpSpawner : MonoBehaviour {

    [SerializeField]
    private HouseItemsCatalog m_houseItemsCatalog;
    [SerializeField]
    private PressAToRestart pressAToRestart;

    //Customize
    [SerializeField]
    private HouseItemSelector m_HouseItemSelector;
    [SerializeField]
    private MpGameplayController.PlayerID m_owner;
    [SerializeField]
    private MPRoomsController m_RoomController;

    //private state
    private int correctAnswers = 0;
    private bool m_canSpawn = false;
    private HouseItem[] m_lastSpawnedItems;

    private const int NUMBER_OF_ITEMS_PER_SELECTION = 3;
    private const float SPAWN_AFTER_TOUCHDOWN_DELAY = 0.3f;

    private void Start()
    {
        GameEventsController.instance.OnGameEventEvent += OnGameEvent;
        MpGameplayController.instance.OnGameplayStart += OnGameplayStart;
        MpGameplayController.instance.OnGameplayEnd += OnGameplayEnd;
        //MpGameplayController.instance.OnRoomTimerStart += OnRoomTimerStart;
        MpGameplayController.instance.OnRoomTimerEnd += OnRoomTimerEnd;
    }

    public void SpawnRandomItem()
    {
        if (!m_canSpawn)
            return;

        //destroy last spawned item if never dispatched by the user! aNIMATE OR SOMETHING IF NEEDED, OR FADEOUT.
        if(m_lastSpawnedItems!=null)
        {
            for(int x=0;x< m_lastSpawnedItems.Length;x++)
            {
                if (m_lastSpawnedItems[x]!=null && m_lastSpawnedItems[x].GetState() == HouseItem.HouseItemState.BeforeSelected)
                {
                    m_lastSpawnedItems[x].AnimateDestroy();
                    Destroy(m_lastSpawnedItems[x].gameObject,1);
                }
            }
            m_lastSpawnedItems = null;
        }

        Debug.Log("Spawner "+ m_owner.ToString() + " SpawnRandomItem=s to select");
        m_lastSpawnedItems = new HouseItem[NUMBER_OF_ITEMS_PER_SELECTION];
        HouseItem[] randomItems = GetRandomItems();//m_houseItemsCatalog.GetRandomHouseItems(NUMBER_OF_ITEMS_PER_SELECTION);

        for(int x=0;x<NUMBER_OF_ITEMS_PER_SELECTION;x++)
        {
            m_lastSpawnedItems[x] = Instantiate(randomItems[x].gameObject, transform.position,Quaternion.identity,transform).GetComponent<HouseItem>();
            //tagg as ours!
            m_lastSpawnedItems[x].SetOwnerID(m_owner);
            //Init as to be grabbed!
            m_lastSpawnedItems[x].StartAsBeforeSelected();
            //inject item to corresponding Player input
        }
        m_HouseItemSelector.SetHouseItemsToSelectFrom(m_lastSpawnedItems);
    }

    //T
    public void SpawnRandomItemForPrize()
    {
        HouseItem item = m_houseItemsCatalog.GetRandomHouseItem();
        HouseItem instantiated = Instantiate(item.gameObject, (Vector2)transform.position + Vector2.up * Random.Range(-1,1.5f) + Vector2.right * Random.Range(-3.0f,3.0f), Quaternion.identity, transform).GetComponent<HouseItem>();
        instantiated.OnReleased();
    }

    public void SpawnRandomItemAfterDelay(float delay)
    {
        if (!m_canSpawn)
            return;
        StartCoroutine(_SpawnRandomItemAfterDelay(delay));
    }

    public IEnumerator _SpawnRandomItemAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        SpawnRandomItem();
    }
    
    private void OnGameEvent(GameEventsController.GameEvent evt, object args)
    {
        switch (evt)
        {
            case GameEventsController.GameEvent.FALLING_OBJECT_TOUCHDOWN:
            case GameEventsController.GameEvent.FALLING_OBJECT_OUTTA_BOUNDS:
                {
                    GameObject go = (GameObject)args;
                    HouseItem ho = go.GetComponent<HouseItem>();
                    if(ho.GetOwnerID() == m_owner && WasRecentlySpawned(ho))
                    {//last dude collided or something?
                        SpawnRandomItemAfterDelay(SPAWN_AFTER_TOUCHDOWN_DELAY);
                    }
                }
                break;
        }
    }

    private bool WasRecentlySpawned(HouseItem ho)
    {
        for(int x=0;x<m_lastSpawnedItems.Length;x++)
        {
            if(m_lastSpawnedItems[x] == ho)
            {
                return true;
            }
        }
        return false;
    }

    private void OnGameplayStart()
    {
    }

    //private void OnRoomTimerStart()
    //{
    //    m_canSpawn = true;
    //    SpawnRandomItem();
    //}

    public void OnRoomReady()
    {
        if (!m_canSpawn)
        {
            m_canSpawn = true;
            SpawnRandomItem();
        }
    }

    private void OnGameplayEnd()
    {
        m_canSpawn = false;
        StopAllCoroutines();
    }
    
    private void OnRoomTimerEnd()
    {
        m_canSpawn = false;
        StopAllCoroutines();
    }

    public void ShowSpawnerScoreForPlayer(bool winner)
    {
        float finalScore = ScoreValidator.Instance.getFinalScore(m_owner);
        StartCoroutine(SpawnScoreCycle(finalScore, winner));
    }

    private IEnumerator SpawnScoreCycle(float finalScore, bool winner)
    {
        int acum = 0;
        MpGameplayController.instance.IsShowingGameScore = true;
        while (acum<finalScore)
        {
            SpawnRandomItemForPrize();
            yield return new WaitForSeconds(0.5f);
            acum += 10;
        }
        //after the showcase spawn the banner with confetti and shit!
        if(winner)
        {
            Instantiate(Resources.Load("WinnerBanner"), transform.position + Vector3.down*5, Quaternion.identity, transform);
            //show press A to restart in a little while
            Invoke("ShowPressAToRestart", 3);
        }
        else
        {
            Instantiate(Resources.Load("LoserBanner"), transform.position + Vector3.down * 6, Quaternion.identity, transform);
        }
    }

    private void ShowPressAToRestart()
    {
        MpGameplayController.instance.IsShowingGameScore = false;
        pressAToRestart.gameObject.SetActive(true);
        pressAToRestart.Activate();
    }

    public MpGameplayController.PlayerID GetOwner()
    {
        return m_owner;
    }

    private HouseItem[] GetRandomItems()
    {
        HouseItem[] result = new HouseItem[NUMBER_OF_ITEMS_PER_SELECTION];

        int currentRoundMaxCorrectAnswers = Random.Range(1, 3);

        for (int i = 0; i < currentRoundMaxCorrectAnswers; i++)
        {
            result[i] = m_houseItemsCatalog.GetRandomHouseItemOfType(m_RoomController.GetCurrentRoomType());
        }

        for (int i = currentRoundMaxCorrectAnswers; i < NUMBER_OF_ITEMS_PER_SELECTION; i++)
        {
            result[i] = m_houseItemsCatalog.GetRandomHouseItemExcludingOfType(m_RoomController.GetCurrentRoomType());
        }

        return result;
    }

}

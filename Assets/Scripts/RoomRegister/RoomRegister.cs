﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomRegister : MonoBehaviour {

	[SerializeField] SandboxSpawner spawner;

    [SerializeField]
    private HouseItemsCatalog m_houseItemsCatalog;

    public List<List<FurnitureProperties>> items;

    void Start()
    {
        items = new List<List<FurnitureProperties>>();
    }

	public void saveHouseItems()
	{
		//Ask Spawner for a list of the references of instantiated house items
		List<FurnitureProperties> propertiesList = new List<FurnitureProperties>();

		List<HouseItem> houseItems = spawner.GetHouseItemList();
        List<GameObject> gameObjects = spawner.GetInstancedObjectsList();

        FurnitureProperties properties;

		for(int i = 0 ; i < houseItems.Count ; i++ )
		{
			properties = new FurnitureProperties(houseItems[i].GetFurnitureType(), gameObjects[i].transform.position, gameObjects[i].transform.rotation);
			propertiesList.Add(properties);
		}

		items.Add(propertiesList);
	}

	public void getHouseItems()
	{
        for (int i = 0; i < items.Count; i++)
        {
            for (int j = 0; j < items[i].Count; j++)
            {
                Debug.Log(string.Format("i [{0}] j [{1}] : Pos[{2}] Id [{3}] Rot [{4}]", i, j, items[i][j].position, items[i][j].houseItemId, items[i][j].rotation));
            }
        }
	}

    public void reconstructLevel(int levelIndex)
    {
        for (int i = 0; i < items[levelIndex].Count; i++)
        {
            //Debug.Log(string.Format("i [{0}] j [{1}] : Pos[{2}] Id [{3}] Rot [{4}]", i, j, items[i][j].position, items[i][j].houseItemId, items[i][j].rotation));
            Instantiate(m_houseItemsCatalog.GetItemOfType(items[levelIndex][i].houseItemId).gameObject, items[levelIndex][i].position, items[levelIndex][i].rotation, transform);
        }
    }

#if DEBUG
    void Update()
    {
        if(Input.GetKeyUp(KeyCode.R))
        {
            saveHouseItems();
        }

        if(Input.GetKeyUp(KeyCode.T))
        {
            //getHouseItems();
            reconstructLevel(0);
        }
    }
#endif
}

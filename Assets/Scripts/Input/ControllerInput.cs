﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public sealed class ControllerInput : MonoBehaviour {

    private enum ControllerState { Idle, DraggingHouseItem };

    #region BOUNDS 
    private class Bounds
    {
        public enum type { TOP, BOTTOM, RIGHT, LEFT};
        public float top;
        public float bottom;
        public float right;
        public float left;
        public Bounds(float top, float bottom, float right, float left)
        {
            this.top = top;
            this.bottom = bottom;
            this.right = right;
            this.left = left;
        }
    }
    #endregion

    //References
    [SerializeField]
    private HouseItemSelector houseItemSelector;
    [SerializeField]
    private Transform spawnerTranform;
    public Animator bottomBoundAnimator;
    private bool blockedInBottom = false;

    //tweakeable state
    [SerializeField]
    private MpGameplayController.PlayerID m_controllerType;
    [SerializeField]
    private bool m_verticalMovement = false;
    [SerializeField]
    private bool m_sandBoxController = true;
    [SerializeField]
    private float m_dragSpeed;
    [SerializeField]
    private float m_movementBoundsOffsetX;
    [SerializeField]
    private float m_movementBoundsOffsetY;
    [SerializeField]
    private float m_boundsRegionToDecelerate;
    

    //private state
    private ControllerState m_state = ControllerState.Idle;
    private HouseItem m_houseItemTarget;

    private Bounds m_movementBounds;

    private bool m_isXAxisInUse = false;

    private void Awake()
    {
        FindObjectOfType<MPRoomsController>().OnHideAndDestroyCurrentRooms += OnHideAndDestroyCurrentRooms;
    }

    // Update is called once per frame
    void Update () {
        //Fire1 is A, Fire2 is B
        switch (m_state)
        {
            case ControllerState.Idle:
                {
                    //pressing A
                    if (IsAButtonDown())//Pressing A
                    {
                        if(m_sandBoxController)
                        {//Debug stuff
                            m_houseItemTarget = GetRandomHouseItemTarget();//Dummy call, to be replaced by a proper selection
                        }
                        else
                        {//get from HouseItemSelector
                            m_houseItemTarget = houseItemSelector.GetSelectedHouseItem();
                        }
                        if (m_houseItemTarget != null)
                        {
                            Debug.Log("[ControllerInput]["+ m_controllerType+"] House item picked up!");
                            m_state = ControllerState.DraggingHouseItem;
                            CalculateMovementBounds();
                            m_houseItemTarget.OnPickedUp();
                        }
                    }
                    else
                    {
                        //Did we try selecting an item with the selector?
                        if (GetHorizontalAxisRaw() != 0)
                        {
                            if (m_isXAxisInUse == false)
                            {
                                // Call your event function here.
                                m_isXAxisInUse = true;
                                houseItemSelector.OnHorizontalAxisPressed(GetHorizontalAxisRaw());
                            }
                        }
                        else if (GetHorizontalAxisRaw() == 0)
                        {
                            m_isXAxisInUse = false;
                        }
                    }
                }
                break;
            case ControllerState.DraggingHouseItem:
                {
                    if (IsAButtonUp())//Releasing A
                    {
                        Assert.IsNotNull(m_houseItemTarget);
                        Debug.Log("[ControllerInput]["+ m_controllerType + "]House item dropped!");
                        m_state = ControllerState.Idle;
                        m_houseItemTarget.OnReleased();
                        m_houseItemTarget = null;
                    }
                    else
                    {
                        Assert.IsNotNull(m_houseItemTarget);
                        //Horizontal movement
                        float horizontalMovement = GetHorizontalAxis();
                        Vector2 targetVel;
                        if(horizontalMovement>0 && CanMoveRight() || horizontalMovement<0 && CanMoveLeft())
                        {
                            float speed = m_dragSpeed * GetSpeedMultiplier(horizontalMovement > 0?Bounds.type.RIGHT:Bounds.type.LEFT);
                            targetVel = Vector2.right * horizontalMovement * speed;
                        }
                        else
                        {//cannot move
                            targetVel = Vector2.zero;
                        }
                        //Vertical debug movement
                        if(m_verticalMovement)
                        {
                            float verticalMovement = GetVerticalAxis();
                            if (verticalMovement > 0 && CanMoveUp() || verticalMovement < 0 && CanMoveDown())
                            {
                                blockedInBottom = false;
                                float speed = m_dragSpeed * GetSpeedMultiplier(verticalMovement > 0?Bounds.type.TOP:Bounds.type.BOTTOM);
                                targetVel += Vector2.up * verticalMovement * speed;
                            }
                            else
                            {
                                if(!blockedInBottom && !CanMoveDown())
                                {
                                    bottomBoundAnimator.SetTrigger("show");
                                    blockedInBottom = true;
                                }
                            }
                        }
                        m_houseItemTarget.SetVelocity(targetVel);
                    }
                }
                break;
        }
    }

    private void OnHideAndDestroyCurrentRooms()
    {
        if( m_state == ControllerState.DraggingHouseItem)
        {
            Assert.IsNotNull(m_houseItemTarget);
            Debug.Log("[ControllerInput][" + m_controllerType + "]House item dropped by force due to room change!");
            m_state = ControllerState.Idle;
            m_houseItemTarget.OnReleased();
            m_houseItemTarget = null;
        }  
    }

    //Dummy debug method, the game will have another thing to acquiere a target
    private HouseItem GetRandomHouseItemTarget()
    {
        if(m_sandBoxController)
        {
            HouseItem[] items = FindObjectsOfType<HouseItem>();
            for (int x = 0; x < items.Length; x++)
            {
                if (!items[x].IsBeingPickedUp())
                {
                    return items[x];
                }
            }
        }
        return null;
    }

    public void SetHouseItemTarget(HouseItem target)
    {
        m_houseItemTarget = target;
    }

    private void CalculateMovementBounds()
    {
        m_movementBounds = new Bounds(
            spawnerTranform.position.y + m_movementBoundsOffsetY, spawnerTranform.position.y - m_movementBoundsOffsetY,
            spawnerTranform.position.x + m_movementBoundsOffsetX, spawnerTranform.position.x - m_movementBoundsOffsetX);
    }

    private float GetSpeedMultiplier(Bounds.type dir)
    {
        switch (dir)
        {
            case Bounds.type.TOP:
                /*if(m_movementBounds.top - m_houseItemTarget.transform.position.y <= m_boundsRegionToDecelerate)
                {
                    return (m_movementBounds.top - m_houseItemTarget.transform.position.y) / m_boundsRegionToDecelerate;
                }*/
                return 1;
            case Bounds.type.BOTTOM:
                return 1;
            case Bounds.type.RIGHT:
                return 1;
            case Bounds.type.LEFT:
                return 1;
        }
        return 1;
    }

    private bool CanMoveRight()
    {
        return m_houseItemTarget.transform.position.x < m_movementBounds.right;
    }

    private bool CanMoveLeft()
    {
        return m_houseItemTarget.transform.position.x > m_movementBounds.left;
    }

    private bool CanMoveUp()
    {
        return m_houseItemTarget.transform.position.y < m_movementBounds.top;
    }

    private bool CanMoveDown()
    {
        return m_houseItemTarget.transform.position.y > m_movementBounds.bottom;
    }


    #region BUTTONS
    private bool IsAButtonDown()//is A down?
    {
        switch (m_controllerType)
        {
            case MpGameplayController.PlayerID.P1:
                return Input.GetButtonDown("Fire1");
            case MpGameplayController.PlayerID.P2:
                return Input.GetButtonDown("Fire1_2");
            default:
                Debug.LogError("Unimplemented controller type");
                return false;
        }
    }
    private bool IsAButtonUp()//is A down?
    {
        switch (m_controllerType)
        {
            case MpGameplayController.PlayerID.P1:
                return Input.GetButtonUp("Fire1");
            case MpGameplayController.PlayerID.P2:
                return Input.GetButtonUp("Fire1_2");
            default:
                Debug.LogError("Unimplemented controller type");
                return false;
        }
    }
    private float GetHorizontalAxis()
    {
        switch (m_controllerType)
        {
            case MpGameplayController.PlayerID.P1:
                return Input.GetAxis("Horizontal");
            case MpGameplayController.PlayerID.P2:
                return Input.GetAxis("Horizontal_2");
            default:
                Debug.LogError("Unimplemented controller type");
                return 0;
        }
    }
    private float GetHorizontalAxisRaw()
    {
        switch (m_controllerType)
        {
            case MpGameplayController.PlayerID.P1:
                return Input.GetAxisRaw("Horizontal");
            case MpGameplayController.PlayerID.P2:
                return Input.GetAxisRaw("Horizontal_2");
            default:
                Debug.LogError("Unimplemented controller type");
                return 0;
        }
    }
    private float GetVerticalAxis()
    {
        switch (m_controllerType)
        {
            case MpGameplayController.PlayerID.P1:
                return Input.GetAxis("Vertical");
            case MpGameplayController.PlayerID.P2:
                return Input.GetAxis("Vertical_2");
            default:
                Debug.LogError("Unimplemented controller type");
                return 0;
        }
    }
    #endregion
    //Buttons 

}

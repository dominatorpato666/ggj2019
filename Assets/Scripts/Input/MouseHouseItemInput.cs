﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class MouseHouseItemInput : MonoBehaviour {

    //private state
    private bool m_isDraggingHouseItem = false;
    private HouseItem m_houseItemDragging;

	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("Fire1"))
        {
            Vector2 mouseWorldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hitInfo = Physics2D.Raycast(mouseWorldPosition, Vector2.zero);
            if (hitInfo.collider != null)
            {
                if(hitInfo.collider.CompareTag("HouseItem"))
                {
                    Debug.Log("[MouseInput] House item picked up!");
                    m_houseItemDragging = hitInfo.collider.gameObject.GetComponent<HouseItem>();
                    m_isDraggingHouseItem = true;
                    m_houseItemDragging.OnPickedUp();
                }
            }
        }
        else if (Input.GetButtonUp("Fire1"))
        {
            if(m_isDraggingHouseItem)
            {
                Assert.IsNotNull(m_houseItemDragging);
                Debug.Log("[MouseInput]House item dropped!");
                m_isDraggingHouseItem = false;
                m_houseItemDragging.OnReleased();
                m_houseItemDragging = null;
            }
        }
        if(m_isDraggingHouseItem)
        {
            Assert.IsNotNull(m_houseItemDragging);
            m_houseItemDragging.SetPosition(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        }
    }
}

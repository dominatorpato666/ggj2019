﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreValidator : MonoBehaviour {

    [SerializeField] float m_rotationLimit = 60f;
    [SerializeField] float m_scoreMultiplier = 10f;

    public static ScoreValidator Instance {get; private set;}

    float m_roomScore1 = 0.0f;
    float m_roomScore2 = 0.0f;
    float m_finalScore1 = 0.0f;    
    float m_finalScore2 = 0.0f;

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        MpGameplayController.instance.OnRoomTimerEnd += OnRoomTimerEnd;
    }

    private void OnRoomTimerEnd()
    {
        Debug.Log("P1 Score: " + getRoomScore(MpGameplayController.PlayerID.P1));
        Debug.Log("P2 Score: " + getRoomScore(MpGameplayController.PlayerID.P2));
        OnLevelCompleted();
    }

#if DEBUG
    void Update()
    {
        if(Input.GetKeyUp(KeyCode.G))
        {
            Debug.Log(getFinalScore(MpGameplayController.PlayerID.P1));
        }

        if (Input.GetKeyUp(KeyCode.H))
        {
            Debug.Log(getFinalScore(MpGameplayController.PlayerID.P2));
        }
    }
#endif

    public float fGetScoreFromAngle(Transform _transform, MpGameplayController.PlayerID owner)
    {
        float result = 0.0f;

        float angleDifference = fGetAngleDifference(_transform);

        //Debug.Log(_transform.gameObject.name + " angle difference: " + angleDifference);

        if(angleDifference <= m_rotationLimit)
        {
            result = 1f;
        }
        else if (angleDifference < m_rotationLimit * 2f)
        {
            result = (1f - (angleDifference / (m_rotationLimit * 2f))) / 2f;
        }

        if (owner == MpGameplayController.PlayerID.P1)
        {
            m_roomScore1 += result;
            m_finalScore1 += result;
        }
        else
        {
            m_roomScore2 += result;
            m_finalScore2 += result;
        }

        return result;
    }

    public float fGetScoreFromAngle(float angleDifference, MpGameplayController.PlayerID owner)
    {
        float result = 0.0f;

        //Debug.Log(_transform.gameObject.name + " angle difference: " + angleDifference);

        if (angleDifference <= m_rotationLimit)
        {
            result = 1f;
        }
        else if (angleDifference < m_rotationLimit * 2f)
        {
            result = 1f - (angleDifference / (m_rotationLimit * 2f));
        }

        if (owner == MpGameplayController.PlayerID.P1)
        {
            m_roomScore1 += result;
            m_finalScore1 += result;
        }
        else
        {
            m_roomScore2 += result;
            m_finalScore2 += result;
        }

        return result;
    }

    public float fGetAngleDifference(Transform _transform)
    {
        float result = 0.0f;

        if (_transform.eulerAngles.z > 90)
        {
            result = _transform.eulerAngles.z - 360f;
        }
        else
        {
            result = _transform.eulerAngles.z;
        }

        return Mathf.Abs(result);
    }

    public float getRoomScore(MpGameplayController.PlayerID owner)
    {
        float result = 0.0f;

        if (owner == MpGameplayController.PlayerID.P1)
        {
            result = m_roomScore1 * m_scoreMultiplier;
        }
        else
        {
            result = m_roomScore2 * m_scoreMultiplier;
        }

        return result;
    }

    public float getFinalScore(MpGameplayController.PlayerID owner)
    {
        float result = 0.0f;

        if (owner == MpGameplayController.PlayerID.P1)
        {
            result = m_finalScore1 * m_scoreMultiplier;
        }
        else
        {
            result = m_finalScore2 * m_scoreMultiplier;
        }

        return result;
    }

    void OnLevelCompleted()
    {
        m_roomScore1 = 0.0f;
        m_roomScore2 = 0.0f;
    }

    public void CleanAll()
    {
        m_roomScore1 = m_roomScore2 = m_finalScore1 = m_finalScore2 = 0.0f;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour {

    [SerializeField] GameObject objInstructions;

    int instructionsSteps = 0;
    
    // Update is called once per frame
    void Update ()
    {
        if (Input.GetKeyUp(KeyCode.R))
        {
            GUI_loadScene(0);
        }

        if (Input.GetKeyUp(KeyCode.Escape))
        {
            quitGame();
        }

        if(Input.GetButtonUp("Fire1") || Input.GetButtonUp("Fire1_2"))
        {
            if (SceneManager.GetActiveScene().buildIndex == 0)
            {
                if (instructionsSteps == 0)
                {
                    objInstructions.SetActive(true);
                    instructionsSteps = 1;
                }
                else if (instructionsSteps == 1)
                {
                    GUI_loadScene(1);
                }
            }
        }
    }

    public void GUI_loadScene(int scene)
    {
        if (SceneManager.GetActiveScene().buildIndex != scene)
        {
            SceneManager.LoadScene(scene);
        }
    }

    public void quitGame()
    {
        Application.Quit();
    }
}

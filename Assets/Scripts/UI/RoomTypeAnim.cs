﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class RoomTypeAnim : MonoBehaviour {

    public Animator animator;

	// Use this for initialization
	void OnEnable () {
        
    }

    public void Show()
    {
        animator.SetBool("inside", true);
        StopAllCoroutines();
        StartDanceAnimation();
    }

    public void Hide()
    {
        StopAllCoroutines();
        animator.SetBool("inside", false);
    }

    private void StartDanceAnimation()
    {
        StartCoroutine(DanceAnimation());
    }

    private IEnumerator DanceAnimation()
    {
        int factor = 1;
        while (true)
        {
            float duration = Random.Range(0.1f, 0.2f);
            transform.DORotate(new Vector3(0, 0, Random.Range(3 * factor, 5 * factor)), duration);
            yield return new WaitForSeconds(duration);
            factor *= -1;
        }
    }
}

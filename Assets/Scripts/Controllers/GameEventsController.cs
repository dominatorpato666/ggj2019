﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEventsController : MonoBehaviour {

    #region SINGLETON
    public static GameEventsController instance;
    private void Awake()
    {
        instance = this;
    }
    #endregion

    public enum GameEvent
    {
        //gameplay
        FALLING_OBJECT_TOUCHDOWN,//When it touches the ground
        FALLING_OBJECT_OUTTA_BOUNDS,
    };

    public delegate void OnGameEvent(GameEvent type, object args);
    public event OnGameEvent OnGameEventEvent;

    public void RaiseEvent(GameEvent type, object args=null)
    {
        if(OnGameEventEvent!=null)
        {
            OnGameEventEvent(type, args);
        }
    }

}

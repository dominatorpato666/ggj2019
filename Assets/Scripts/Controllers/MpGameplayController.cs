﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public sealed class MpGameplayController : MonoBehaviour {

    #region SINGLETON
    public static MpGameplayController instance;
    private void Awake()
    {
        instance = this;
    }
    #endregion

    public enum PlayerID { P1, P2 };

    //Constants
    private const int ROOM_DURATION = 15;//seconds
    private const int ROOM_END_DELAY = 2;//seconds
    private const int NUMBER_OF_ROUNDS = 3;

    //private state
    public bool IsInGameplay { get; private set; }
    private int currentRound = 0;

    private MpSpawner[] mpSpawners;
    [NonSerialized]
    public MPRoomsController mpRoomsController;

    //events for observers
    public event Action OnGameplayStart;
    public event Action OnGameplayEnd;

    public event Action OnRoomTimerStart;
    public event Action<float> OnRoomTimerTick;//sends normalized progress
    public event Action OnRoomTimerEnd;

    public bool IsShowingGameScore = false;

    private void Start()
    {
        mpSpawners = FindObjectsOfType<MpSpawner>();
        mpRoomsController = FindObjectOfType<MPRoomsController>();
        StartGameplay();
    }

    public void StartGameplay()
    {
        if(!IsInGameplay)
        {
            IsInGameplay = true;
            if (OnGameplayStart != null)
                OnGameplayStart();
            StartRoomTimer();
            currentRound = 0;
        }
    }

    public void StopGameplay()
    {
        IsInGameplay = false;
        if (OnGameplayEnd != null)
            OnGameplayEnd();
    }

    public void RestartGameplay()
    {
        StartCoroutine(RestartGameplayAfterCleaning());
    }

    private IEnumerator RestartGameplayAfterCleaning()
    {
        mpRoomsController.HideAndDestroyCurrentRooms();
        //find winner winner and destroy it.
        WinnerBanner banner = FindObjectOfType<WinnerBanner>();
        Destroy(banner.gameObject);
        LoserBanner loserbanner = FindObjectOfType<LoserBanner>();
        Destroy(loserbanner.gameObject);
        //clean the Score Validator
        ScoreValidator.Instance.CleanAll();
        yield return new WaitForSeconds(3);
        //Also destryo any remaining furniture
        HouseItem[] items = FindObjectsOfType<HouseItem>();
        foreach (HouseItem item in items)
        {
            Destroy(item.gameObject, 1);
            item.AnimateDestroy();
        }
        StartGameplay();
    }

    //Time management
    private void StartRoomTimer(float delay = 0)
    {
        StartCoroutine(RoomTimerCoroutine(ROOM_DURATION, delay));
    }
    
    private IEnumerator RoomTimerCoroutine(int duration, float delay)
    {
        yield return new WaitForSeconds(delay);
        if (OnRoomTimerStart != null)
            OnRoomTimerStart();
        Debug.Log("OnRoomTimerStart");
        float elapsed = 0;
        while(elapsed<duration)
        {
            elapsed += Time.deltaTime;
            if (OnRoomTimerTick != null)
                OnRoomTimerTick(elapsed/duration);
            yield return null;
        }
        //End timer
        OnRoomTimerEnds();
    }

    private void OnRoomTimerEnds()
    {
        Debug.Log("OnRoomTimerEnd");
        if (OnRoomTimerEnd != null)
            OnRoomTimerEnd();
        currentRound++;
        if(currentRound>=NUMBER_OF_ROUNDS)
        {
            //game end! show scores and yada yada!
            StopGameplay();
            StartCoroutine(ShowGameWinner(2));
        }
        else
        {
            //Now a delay and a new Room!
            StartRoomTimer(ROOM_END_DELAY);
        }
    }

    private IEnumerator ShowGameWinner(float delay)
    {
        yield return new WaitForSeconds(delay);
        mpRoomsController.SpawnNewRooms();
        //calculate the winner 
        float p1Score = ScoreValidator.Instance.getFinalScore(PlayerID.P1);
        float p2Score = ScoreValidator.Instance.getFinalScore(PlayerID.P2);
        PlayerID winner = PlayerID.P1;
        if (p2Score > p1Score)
            winner = PlayerID.P2;
        foreach (MpSpawner s in mpSpawners)
        {
            s.ShowSpawnerScoreForPlayer(s.GetOwner() == winner);
        }
    }

    private void HideGameWinner()
    {

    }

    //Cheat to start gameplay, remove later and so on
    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Space))
        {
            StartGameplay();
        }
    }

}
